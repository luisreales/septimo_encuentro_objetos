/* Definir clase Auto*/
class Auto {
    constructor (marca,color, cantPuertas) {
        this.marca = marca;
        this.color = color;
        this.cantPuertas = cantPuertas;
    }
    /* --- Metodo --- */
    mostrarMarca(){
        return `El auto ${this.marca}`
    }
    mostrarMensaje() {
        console.log(`Es de color: ${this.color}`);
        return (`Es de color: ${this.color}`)
    }
    mostrarCantPuertas() {
        return (`tiene ${this.cantPuertas} puertas.`)
    }
}

let autoUno = new Auto('Fiat','Rojo',3);
let autoDos = new Auto('Mercedes Benz','Verde',5);

document.getElementById('auto1').innerHTML = autoUno.mostrarMarca() + ", " + autoUno.mostrarMensaje() + " y " + autoUno.mostrarCantPuertas();
document.getElementById('auto2').innerHTML = autoDos.mostrarMensaje() + " - " + autoDos.mostrarCantPuertas();

/* Creando la clase User con sus propiedades y metodos */
class User{
    constructor(name, lastName){
        this._name = name;
        this._lastName = lastName;
    }
    //GETTER
    get name() {
        return this._name;
    }
    //SETTEER
    set name(value) {
        this._name = value;
    }
    //GETTER
    get lastName() {
        return this._lastName;
    }
    //SETTEER
    set lastName(value) {
        this._lastName = value;
    }
    //getter
    get fullname(){
        return `${this.name} ${this.lastName}`
    }

    //Metodo publico
    obtenerAñoProduccion(){
        return 1960;
    }
    //Metodo estatico
    static informacionClase(){
        return `Esta clase es para almacenar informacion de usuarios`
    }
}

let user1  = new User('Pedro','Picapiedra');
console.log(user1.fullname);
user1.name = 'Pablo';
user1.lastName = 'Marmol';
console.log(user1.fullname);

console.log ('Los picapiedras se crearon en el año '+ user1.obtenerAñoProduccion())

/* Herencia */
class Animal {
    constructor(nombre){
        this.nombre = nombre;
    }
    hablar(){
        console.log(`${this.nombre} hace un ruido`)
    }
}

class Perro extends Animal  {
    hablar(){
        console.log(`${this.nombre} Ladrar Guau Guau`)
    }
}

class Gato extends Animal {
    hablar(){
        console.log(`${this.nombre} Maullar Miau Miau`)
    }
}

let perro1 = new Perro('Lazzie ');
perro1.hablar();
let perro2 = new Perro('Bucky ');
perro2.hablar();

let gato1 = new Gato('Tom ');
gato1.hablar();