class Person{
    constructor(name, lastName, age){
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }
    fullName(){
        console.log(`${this.name} ${this.lastName}`);
    }
    es_mayor(){
        if (this.age < 18){
            return false;
        } 
        else {
            return true;
        }
    }
}

let onePerson = new Person('Marcelo','Quiroga', 29);
console.log(onePerson.fullName());
console.log(onePerson.es_mayor())