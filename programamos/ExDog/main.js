/* Clase Perro */
class Perro{
    constructor(nombre, raza, edad, color, estadoAdopcion){
        this.nombre = nombre;
        this.color = color;
        this.raza = raza;
        this.edad = edad;
        this._estadoAdopcion = estadoAdopcion;
    }
    //Setter
    set estadoAdopcion(value){
        return this._estadoAdopcion = value;
    }
    //Getter
    get estadoAdopcion() {
        return this._estadoAdopcion
    }
    }
let perros = []

/* Ingresar datos de los perros */
do{
    let nombre = prompt('Ingrese el nombre del perro');
    let raza = prompt('Ingrese la raza:');
    let edad = prompt('Ingrese la edad:')
    let color = prompt('Ingrese el color:')
    let estadoAdopcion = prompt('Ingrese el estado de adopción (0: En Adopcion - 1: En proceso de adopcion - 2: Adoptados):')
    perros.push(new Perro(nombre,raza, edad, color, estadoAdopcion));
}while(window.confirm('Desea Cargar otro Perro'));

/* Listado de todos los perros */
console.log('Todos los perros:')
for (let i = 0; i <perros.length;i++){
    console.log(`Nombre: ${perros[i].nombre} / Raza: ${perros[i].raza}`)
}

function listarEstado(estado){
    for (let i = 0; i <perros.length;i++){
        if (perros[i].estadoAdopcion == estado){
            console.log(`Nombre: ${perros[i].nombre} / Raza: ${perros[i].raza}`)
        }
    }
}
/**********************************/
/* Listado de perros en adopcion */
/********************************/ 
console.log('Todos los perros en adopcion:')
listarEstado(0);

/*********************************************/
/* Listado de perros en proceso de adopcion */
/*******************************************/
console.log('Todos los perros en procesos de adopcion:')
listarEstado(1);

/********************************/
/* Listado de perros adoptados */ 
/******************************/
console.log('Todos los perros adoptados:')
listarEstado(2);


/* Modificar Estado */
// alert (`El estado de adopcion de ${perros[0].nombre} es: ${perros[0].estadoAdopcion}`)
// perros[0].estadoAdopcion = prompt(`Modificar el estado de ${perros[0].nombre}`)

/* Consultar Estado */
// alert (`El nuevo estado es ${perros[0].estadoAdopcion}`)

